# pre_log.js

A very tiny script to log preformatted text on a webpage. I use it to prototype
ASCII tools and games.

When embed in a webpage, pre_log.js create a `<pre>` element with `log` as log display.

There is two functions :

`log(str)`

Add string ending with new line to log

`clear()`

Empty log

## License notice

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
